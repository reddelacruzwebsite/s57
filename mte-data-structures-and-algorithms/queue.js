let collection = [];

// Write the queue functions below.

const print = function () {
	   return collection
}

const enqueue = function (name) {

	 collection.push(name)
	 return collection

}

const dequeue = function () {

	 collection.shift()
	 return collection

}

const size = function () {
	 return collection.length;
}

const front = function () {
	 return collection[0];
}

const isEmpty = function () {
	 return collection.length == 0;
}



module.exports = {

	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};